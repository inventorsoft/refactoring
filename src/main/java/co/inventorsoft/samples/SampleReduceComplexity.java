package co.inventorsoft.samples;


import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * task print all simple numbers, which doesn't contain 3, 4, 5, 6, 7
 * max number could not be divided on 3, 6, 10
 */
@Slf4j
public class SampleReduceComplexity {

	private static final List<Integer> IGNORED_NUMBERS = Arrays.asList(3,4,5,6,7);

	public static void main(String... arg) {
		printSimpleNumbers(9998);
	}

	private static void printSimpleNumbers(int maxNumber) {
		//validate data
		validate(maxNumber);

		//search for simple numbers
		List<Integer> result = getSimpleNumbers(maxNumber);

		result = filterResult(result, IGNORED_NUMBERS);

		log.debug("Simple numbers: {}", result);
	}

	private static void validate(int maxNumber) {
		if (maxNumber == 0) {
			throw new IllegalArgumentException("Max number could not be 0");
		} if (maxNumber < 0) {
			throw new IllegalArgumentException("Max number could not be negative");
		} if (maxNumber % 3 == 0) {
			throw new IllegalArgumentException("Max number could not be divided on 3");
		} if (maxNumber % 6 == 0) {
			throw new IllegalArgumentException("Max number could not be divided on 6");
		} if (maxNumber % 10 == 0) {
			throw new IllegalArgumentException("Max number could not be divided on 10");
		}
	}

	private static List<Integer> getSimpleNumbers(int maxNumber) {
		List<Integer> result = new ArrayList<>();
		for (int index = 2; index <= maxNumber; index++) {
			boolean isSimple = true;
			for (Integer aResult : result) {
				if (index % aResult == 0) {
					isSimple = false;
				}
			}
			if (isSimple) {
				result.add(index);
			}
		}
		return result;
	}

	private static List<Integer> filterResult(List<Integer> source, List<Integer> ignoredNumbers) {
		return source.stream().filter(item-> {
			String numberString = Integer.toString(item);
			long count = ignoredNumbers
					.stream()
					.filter(num-> numberString.contains(Integer.toString(num)))
					.count();
			return count == 0;
		}).collect(Collectors.toList());
	}
}
