package co.inventorsoft.samples;


import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Sample return statement, collections, default case
 */
//todo: discuss methods order
@Slf4j
public class SampleOne {

	private static final List<String> GREEN_COLORS = Arrays.asList("#00ff00", "#00e500", "#00cc00", "#00b200", "#009900", "#007f00", "#006600", "#004c00");

	public static void main(String... args) {

	}

	/**
	 * Get color hash codes based on the color name
	 *
	 * @param source color name
	 * @return list of color codes
	 */
	private static List<String> getColorHashCodes(Color source) {
		List<String> result;
		switch (source) {
			case GREEN:
				result = GREEN_COLORS;
				break;
			case BLUE:
				result = Arrays.asList("#0000ff", "#0000e5", "#0000cc", "#0000b2", "#000099", "#00007f");
				break;
			case RED:
				result = Arrays.asList("#e50000", "#cc0000", "#b20000", "#00b200", "#990000", "#7f0000", "#660000");
				break;
			case PURPLE:
				result = new ArrayList<>();
				break;
			default:
				throw new IllegalArgumentException("uknown color " + source.name());
		}
		return result;

	}

	/**
	 * detrmines if color is green or not
	 *
	 * @param color source color
	 * @return true if green, false otherwise
	 */
	private static boolean isGreen(String color) {
		return "green".equalsIgnoreCase(color);
	}

	private SampleOne() {
		Color color = Color.PURPLE;
		List<String> colors = getColorHashCodes(color);
		log.debug("Is {} green ? A: {}", color, isGreen(colors.get(0)));
	}

	private static Color getColor(String hashCode) {
		//todo: refactor if statements
		if (GREEN_COLORS.contains(hashCode)) {
			return Color.GREEN;
		} else {
			//todo: add code for other colors.
			return null;
		}
	}

	private enum Color {
		GREEN,
		BLUE,
		RED,
		PURPLE,
		WHITE
	}
}
