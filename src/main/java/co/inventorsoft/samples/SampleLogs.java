package co.inventorsoft.samples;

import co.inventorsoft.samples.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

//todo: discuss log levels,
//restriction on logs
@Slf4j
public class SampleLogs {

	public static void main (String... args) {
		printUserData();
	}

	public static void printUserData() {
		ClassLoader classLoader = SampleLogs.class.getClassLoader();
		URL userUrl = classLoader.getResource("user.json");
		if (userUrl == null) {
			log.error("Could not found user.json file.");
		} else {
			File userJsonFile = new File(userUrl.getFile());
			try {
				String userJson = FileUtils.readFileToString(userJsonFile, "UTF-8");
				ObjectMapper objectMapper = new ObjectMapper();
				User user = objectMapper.readValue(userJson, User.class);
				log.info("User data: {}", user);
			} catch (IOException ex) {
				log.warn("Could not read user json from file.", ex);
			}
		}

	}


}
