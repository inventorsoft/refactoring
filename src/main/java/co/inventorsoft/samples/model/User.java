package co.inventorsoft.samples.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {
	private Integer id;
	private String email;
	private String password;

	@Override
	public String toString() {
		return String.format("id: %d, email: %s", id, email);
	}
}